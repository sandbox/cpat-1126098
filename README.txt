-- SUMMARY --
-------------
Block content module derivate Block core module to add block management interface on node context. 


-- FEATURES --
--------------
 - Authors with 'create and manage block context' permission can view 'Manage Blocks context' tab on node. On this interface, they can add new blocks on a node. To configure this new block, author have to register, block info, title, body and choose if the block could be re-use on other node context. In an editorial context, visibility settings are not displayed.
 
 - Then, blocks created and blocks which could be re-use are displayed on blocks management interface (the same that de core blocks module one) for the contextual node. Author choose the region to display the block and blocks order, with the drag-and-drop interface.
 
 - Users with 'administer block context' permission can choose content-types where block context will be apply. 


-- INSTALLATION --
------------------
1) Copy block_context directory to your modules directory
2) Enable the module at: /admin/build/modules
3) Configure permissions at: /admin/user/permissions
4) Edit the settings at: /admin/settings/block-context


-- CONTACT --
-------------
Current maintainer:
  * C.Patrix (Makina Corpus, France) - http://drupal.org/user/786984
  
This project has been sponsored by:
  * Makina Corpus
    Friendly Drupal experts providing professional consulting & training.
    Visit http://www.makina-corpus.com for more information.
